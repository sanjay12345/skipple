import { React } from "react";
 
 

function Textinput(props) {
  
    return(
    <>
    <input
    type={props.inputtype}
    placeholder={props.inputplaceholder}
    className="inputbox"

    onChange={(e) => props.onChange(e.target.value)}
     />
     </> 
  )
}
 

export default Textinput;